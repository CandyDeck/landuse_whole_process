""" FAO land use processing

Note
-----

Runs the full sequence of FAO data downloading and processing:

CD - need to rename based on the folder names
CD - and be consistent with the naming of th e
1) Downloading the data 
2) Raw data processing
3) Exio sector aggregation
4) 

"""

from pathlib import Path

import sys
sys.path.insert(1, 'download')
import download.main

# All settings for running the script
DATAFOLDER: Path = Path('/home/candy/tmp/FAO')

STARTYEAR: int = 1995
ENDYEAR: int = 1997
YEARS = range(STARTYEAR, ENDYEAR+1)

# Preperations
DATAFOLDER.mkdir(exist_ok=True, parents=True)

# Step 1 - downloading the data
download.main.get_all(years=YEARS, storage_path=DATAFOLDER)
# Step 2 
#raw_data_processing.main.get_all(years=YEARS, storage_path=DATAFOLDER)
# ....

