from pathlib import Path
import pandas as pd
import yaml
from make_years import make_valid_fao_year as mvy
import fill_country_area as fca
import zero_assumption as za


import case1 
import case2
import case3
import case4
import case_small_diagrams as csd
import adjustment as adj
import regression_implant as reg
import regression_implant2 as reg2







#import regression_implant3 as reg3
#import fill_cells
#import adjustment2 as adj2

'''
import glob
import yaml
files = glob.glob("*.yaml") # list of all .yaml files in a directory 
new_string=[]
fichier=[]

for file  in files:
    print(file)
    new_string.append(file.replace(".yaml", ""))
    print(new_string)
for file in range(len(files)):
    with open(files[file]) as data:
         #fichier.append(yaml.load(data, Loader=yaml.FullLoader)
         dictionary_data = yaml.safe_load(data)
print(new_string[1]+ '%s %d' % (parameters,len(files)))
'''



with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)

with open(r'items_primary.yaml') as file:
    items_primary = yaml.load(file, Loader=yaml.FullLoader)
    
with open(r'diagram.yaml') as file:
    diagram = yaml.load(file, Loader=yaml.FullLoader)


with open(r'unique_items.yaml') as file:
    unique_items = yaml.load(file, Loader=yaml.FullLoader)    
    
with open(r'small_diagrams.yaml') as file:
    small_diagrams = yaml.load(file, Loader=yaml.FullLoader) 

with open(r'items_small_diagrams.yaml') as file:
    items_small_diagrams = yaml.load(file, Loader=yaml.FullLoader) 
    
with open(r'country.yaml') as file:
    country = yaml.load(file, Loader=yaml.FullLoader) 
    


relevant_years = [mvy(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]


storage_root = Path("../../land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"
      
landuse = pd.read_csv('../../../../../../tmp/FAO/data/refreshed_land_use.csv', encoding="latin-1") 

list_item_code = list(landuse['Item Code'])
FAOitem = []
for i in list_item_code:
    if i not in FAOitem:
        FAOitem.append(i)

col_years = [col for col in landuse.columns if  col.startswith("Y")] 
  
meta_col = ["ISO3", "Item Code", "Item","Unit"] 
        
landuse=landuse[meta_col + relevant_years]
landuse = landuse[landuse['Unit'] != 'million tonnes']
landuse = landuse[landuse['ISO3'] != 'not found']

landuse=landuse[landuse.ISO3.isin(country)]
landuse.isnull().sum().sum()

'''
Here we make sure that the country area is available for the relevant years of
each country
'''


for code in country :
    fca.fill(landuse, code,col_years,relevant_years,parameters)


'''
Here, dictionnaries are created.
We list for each major items the minor item corresponding
'''


dfs = dict()

for key in diagram:
    df1 = pd.DataFrame(columns = ['ISO3','item'])
    for year in relevant_years:
        df1[year]=""
    for code in country :
       for item in list(diagram.get(key).values()) : 
           df1 = df1.append(pd.Series([code,item,0], index=['ISO3','item',year]),ignore_index=True)
    df1=df1.fillna(0)
    dfs[key] = df1.copy()

for key in small_diagrams:
    df1 = pd.DataFrame(columns = ['ISO3','item'])
    for year in relevant_years:
        df1[year]=""
    for code in country :
       for item in list(small_diagrams.get(key).values()) : 
           df1 = df1.append(pd.Series([code,item,0], index=['ISO3','item',year]),ignore_index=True)
    df1=df1.fillna(0)
    dfs[key] = df1.copy()
 
    
    
    
za.assumption(country, FAOitem, parameters, landuse,col_years) #13:27 start - 13:55 end#
landuse.isnull().sum().sum()
                            
landuse.to_csv('etapezero.csv',index = False)
   

for code in country :
    #15:16 begins - 17:04 end
    print(code)
    if not code in parameters.get("exeptions"):
        relevant_years = [mvy(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [mvy(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
    
    for key in diagram:
        print(key)
        missing=0
        year1b=year2b=year3b=parameters.get("year_of_interest").get("begin")
        year1e=year2e=year3e=parameters.get("year_of_interest").get("end")
        a=[]
        if diagram.get(key).get("minor1") in parameters.get("exeptions"):
            exeption1=diagram.get(key).get("minor1")
            year1b=parameters.get("exeptions").get(exeption1).get("begin")
            year1e=parameters.get("exeptions").get(exeption1).get("end")
            a.append(year1b)
        if diagram.get(key).get("minor2") in parameters.get("exeptions"):
            exeption2=diagram.get(key).get("minor2")
            year2b=parameters.get("exeptions").get(exeption2).get("begin")
            year2e=parameters.get("exeptions").get(exeption2).get("end")
            a.append(year2b)
        if diagram.get(key).get("minor3") in parameters.get("exeptions"):
            exeption3=diagram.get(key).get("minor3")
            year3b=parameters.get("exeptions").get(exeption3).get("begin")
            year3e=parameters.get("exeptions").get(exeption3).get("end")
            a.append(year3b)
          
        #CASE1 which correspond to a case where neither the country or the FaoItem is in the exeption list
        if not a and not code in parameters.get("exeptions"):
            case1.solve(landuse, dfs,code,relevant_years, diagram,key,country,missing)
        
        #CASE2 which correspond to a case where only the country is in the exeption list
        if a and code not in parameters.get("exeptions"):
            case2.solve(landuse, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a,parameters)
        
        #CASE3 which correspond to a case where only the FaoItem is in the exeption list
        if not a and code in parameters.get("exeptions"):
            case3.solve(landuse, dfs,code,relevant_years, diagram,key,country,missing)
        
        #CASE4 which correspond to a case where the country and the FaoItem are in the exeption list
        if a and code in parameters.get("exeptions"):
            case4.solve(landuse, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a,parameters)

landuse[col_years] = landuse[col_years].apply(pd.to_numeric)
landuse.to_csv('2eme_etape.csv',index = False)


'''
Linear interpolartion or primary items # 1min
'''


landuse=landuse.reset_index().set_index(meta_col)
for code in country :
    landuse_new=landuse[(landuse.index.get_level_values(0)==code)&(landuse.index.get_level_values(1).isin(items_primary))][col_years].interpolate(method ='linear',axis=1,limit_area ='inside')
    for item in landuse_new.index:
        if item in landuse.index:
            landuse.loc[item,col_years]=landuse_new.loc[item, col_years]

landuse=landuse.reset_index()
landuse.to_csv('linear_interpolation_item_primary.csv',index = False)   

'''
adjust major = sum(minor) begin 17:19, end :17:41
'''
for code in country :
    print(code)
    if not code in parameters.get("exeptions"):
        relevant_years = [mvy(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [mvy(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in diagram:
        for years in relevant_years :
            adj.adjust(landuse, dfs,code,years, diagram,key,country)
        #landuse.to_csv('adj_primary_items.csv',index = False)
        
landuse.to_csv('adj_after_linear_interpolation.csv',index = False)   
        
'''
regression begin : 17:43 end 17:51
'''

for code in country :
    reg.regression(code,parameters,landuse)
landuse.to_csv('regression_primary_items.csv',index = False)   

'''
Adjustment after regression   begin 17:55 end : 18:16
'''


for code in country :
    if not code in parameters.get("exeptions"):
        relevant_years = [mvy(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [mvy(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in diagram:
        for years in relevant_years :
            adj.adjust(landuse, dfs,code,years, diagram,key,country)
        landuse.to_csv('land_use_adjust2.csv',index = False)

landuse.to_csv('adjust_after_regression_primary_items.csv',index = False)   
        
        
'''--------------------------------------------------------------------------------------------------------------'''  
'''
linear interpolation and regression for unique items
This should be run in parallel of the first part of the script
start 19:08 end : 19:20
'''     

landuse=landuse.reset_index().set_index(meta_col)
for code in country :
    print(code)
    landuse_new=landuse[(landuse.index.get_level_values(0)==code)&(landuse.index.get_level_values(1).isin(unique_items))][col_years].interpolate(method ='linear',axis=1,limit_area ='inside')
    for item in landuse_new.index:
        if item in landuse.index:
            landuse.loc[item,col_years]=landuse_new.loc[item, col_years]

landuse=landuse.reset_index()
landuse.to_csv('interpolation_uniqueItem.csv',index = False)

 
  
for code in country :
    reg2.regression(code,parameters,landuse,unique_items)
landuse.to_csv('regression_uniqueItem.csv',index = False)

    
'''--------------------------------------------------------------------------------------------------------------'''   
'''
This part deals with the small diagrams
could be run in parallel of the 2 first parts

''' 

'''
20:16 begin calculation
'''

for code in country :
    print(code)

    for key in small_diagrams:
        missing=0
        print(key)            
        csd.solve(landuse, dfs,code,relevant_years, small_diagrams,key,country,missing,parameters)
landuse.to_csv('calculation_small_diagrams.csv',index = False)            


'''
Interpolation small diagrams
'''          
landuse=landuse.set_index(meta_col)
for code in country :
    print(code)
    landuse_new=landuse[(landuse.index.get_level_values(0)==code)&(landuse.index.get_level_values(1).isin(items_small_diagrams))][col_years].interpolate(method ='linear',axis=1,limit_area ='inside')
    for item in landuse_new.index:
        if item in landuse.index:
            landuse.loc[item,col_years]=landuse_new.loc[item, col_years]

landuse=landuse.reset_index()
landuse.to_csv('fill_items_small_diagrams.csv',index = False)            
#1min#

'''
Adjust small diagrams
'''
for code in country :
    print(code)
    if not code in parameters.get("exeptions"):
        relevant_years = [mvy(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [mvy(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in small_diagrams:
        print(key)
        for years in relevant_years :
            adj.adjust(landuse,dfs,code,years, small_diagrams,key,country)
        #landuse.to_csv('land_use_adjust4.csv',index = False)
landuse.to_csv('adjust_small_diagram1.csv',index = False)
#1min#  
'''
Regression small diagram
'''
for code in country :
    reg2.regression(code,parameters,landuse,items_small_diagrams)
landuse.to_csv('regression_small_diagram1.csv',index = False)

'''
Adjust after regression
'''

for code in country :
    print(code)
    if not code in parameters.get("exeptions"):
        relevant_years = [mvy(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [mvy(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in small_diagrams:
        print(key)
        for years in relevant_years :
            adj.adjust(landuse,dfs,code,years, small_diagrams,key,country)
        #landuse.to_csv('land_use_adjust4.csv',index = False)
landuse.to_csv('adjust_after_regression_small_diagram1.csv',index = False)        
        
        
landuse.to_csv('final.csv',index = False)

